<div class="row">
    <div class="col-6">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Buat Data User</h4>
                    <?php echo form_open('user_save'); ?>
                    <div class="form-group">
                        <label for="nip">NIP</label>
                        <input type="text" class="form-control" id="nip" name="nip" placeholder="nip">
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="nama lengkap">
                    </div>
                    <div class="form-group">
                        <label class="label">Username</label>
                        <input type="text" class="form-control" placeholder="username" name="username">
                    </div>
                    <div class="form-group">
                        <label class="label">Password</label>
                        <input type="password" class="form-control" placeholder="password" name="password">
                    </div>
                    <div class="form-group">
                        <label class="label">Ulangi Password</label>
                        <input type="password" class="form-control" placeholder="ulangi password" name="repassword">
                    </div>
                    <div class="form-group">
                        <label class="label">Level</label>
                        <select name="level" class="form-control">
                            <option>Pilih</option>
                            <option value="admin">Admin</option>
                            <option value="operator">Operator</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo site_url('user') ?>">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>