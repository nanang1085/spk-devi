<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">

        <div class="card-body">
            <h4>Data User</h4><br>
            <?php
            if ($this->session->flashdata('err_message')) { ?>
                <div class="<?php echo $this->session->flashdata('err_message')['class']; ?>">
                    <?php echo $this->session->flashdata('err_message')['message']; ?>
                </div>
            <?php } ?>
            <a href="<?php echo site_url('user_add'); ?>" class="btn btn-sm btn-primary mb-3">Tambah Data</a>
            <table class="tabel table display compact nowrap">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> NIP </th>
                        <th> Username </th>
                        <th> Nama </th>
                        <th> Level </th>
                        <th style="width: 10%;"> Aksi </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($users as $k) {
                    ?>
                        <tr>
                            <td scope="row"><?php echo $no++; ?></td>
                            <td><?php echo $k['NIP'] ?></td>
                            <td><?php echo $k['Username'] ?></td>
                            <td><?php echo $k['Nama'] ?></td>
                            <td><?php echo $k['level'] ?></td>
                            <td>
                                <?php
                                if ($this->session->userdata('level') == 'admin') {
                                ?>
                                    <a class="badge badge-danger" href="<?php echo site_url('user_delete/' . $k['NIP']); ?> " onclick="return(confirm('Data ini ingin dihapus?'))">Hapus</a>
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>