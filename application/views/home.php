<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="d-flex">
                            <div class="wrapper">
                                <h3 class="mb-2 font-weight-semibold">Halo <?= $this->session->userdata('username') ?>,</h3>
                                <h5 class="mb-1 font-weight-medium text-primary">Selamat Datang di Sistem Pendukung Keputusan Penentuan Siswa Berprestasi.</h5>
                                <h5><i>(Studi Kasus: SLTA di Kecamatan Rengat Barat) Dinas Pendidikan Provinsi Riau Cabang IV.</i></h5>
                            </div>
                            <!-- <div class="wrapper my-auto ml-auto ml-lg-4">
                                <div class="chartjs-size-monitor" style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                    <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                        <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                        <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                    </div>
                                </div>
                                <canvas height="50" width="100" id="stats-line-graph-1" class="chartjs-render-monitor" style="display: block;"></canvas>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
