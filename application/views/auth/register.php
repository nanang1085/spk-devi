<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Login - Sistem Pendukung Keputusan</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/asssets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href=".<?php echo base_url(); ?>/assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/shared/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/images/favicon.ico" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
              <h4 class="mb-3">Registrasi Akun</h4>
              <?php
              if ($this->session->flashdata('err_message')) { ?>
                <div class="alert alert-warning">
                  <?php
                  echo $this->session->flashdata('err_message')['message'];
                  ?>
                </div>
              <?php
              }
              echo form_open('registersave', ['class' => 'card']); ?>
              <div class="form-group">
                <label class="label">NIP</label>
                <input type="text" class="form-control" placeholder="nip" name="nip">                
              </div>
              <div class="form-group">
                <label class="label">Nama Lengkap</label>
                <input type="text" class="form-control" placeholder="nama lengkap" name="nama">                
              </div>
              <div class="form-group">
                <label class="label">Username</label>
                <input type="text" class="form-control" placeholder="username" name="username">                
              </div>
              <div class="form-group">
                <label class="label">Password</label>
                <input type="password" class="form-control" placeholder="password" name="password">                
              </div>
              <div class="form-group">
                <label class="label">Ulangi Password</label>
                <input type="password" class="form-control" placeholder="ulangi password" name="repassword">                
              </div>
              <div class="form-group">
                        <label class="label">Level</label>
                        <select name="level" class="form-control">
                            <option>Pilih</option>
                            <option value="admin">Admin</option>
                            <option value="operator">Operator</option>
                        </select>
                    </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary submit-btn btn-block">Login</button>
              </div>
              
              <div class="text-block text-center my-3">
                <span class="text-small font-weight-semibold">Belum punya akun?</span>
                <a href="<?= site_url('register') ?>" class="text-black text-small">Registrasi Akun</a>
              </div>
              </form>
            </div>

          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?php echo base_url(); ?>/assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url(); ?>/assets/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?php echo base_url(); ?>/assets/js/shared/off-canvas.js"></script>
  <!-- endinject -->
  <script src="<?php echo base_url(); ?>/assets/js/shared/jquery.cookie.js" type="text/javascript"></script>
</body>

</html>