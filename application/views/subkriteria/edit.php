<div class="row">
    <div class="col-4">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Ubah Data Range</h4>

                    <?php echo form_open('subkriteria-update'); ?>

                    <input type="hidden" name="id_sub_kriteria" value="<?= $dks[0]['id_sub_kriteria'] ?>">

                    <div class="form-group">
                        <label for="id_kriteria">Kriteria</label>
                        <select class="form-control" name="id_kriteria">
                            <?php foreach ($data_kriteria as $val) { ?>
                                <option value="<?= $val['id_kriteria'] ?>" <?= $val['id_kriteria'] == $dks[0]['id_kriteria'] ? 'selected' : '' ?>>
                                    <?= $val['Nama_kriteria'] ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sub_kriteria">Nama Range</label>
                        <input type="text" class="form-control" id="sub_kriteria" name="sub_kriteria" value="<?= $dks[0]['sub_kriteria'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="nilai">Nilai</label>
                        <input type="number" class="form-control" id="nilai" name="nilai" value="<?= $dks[0]['nilai'] ?>">
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo site_url('subkriteria') ?>">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
