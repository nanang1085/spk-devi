<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">

        <div class="card-body">
            <h4>Data Range</h4><br>
            <?php
            if ($this->session->flashdata('err_message')) { ?>
                <div class="<?php  echo $this->session->flashdata('err_message')['class']; ?>">
                    <?php  echo $this->session->flashdata('err_message')['message']; ?>
                </div>
            <?php } ?>
            <a href="<?php echo site_url('subkriteria-add'); ?>" class="btn btn-sm btn-primary mb-3">Tambah Data</a>
            <table class="tabel table display compact nowrap">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> Kriteria </th>
                        <th> Nama Range </th>
                        <th> Nilai </th>
                        <th style="width: 10%;"> Aksi </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($data_subkriteria as $k) {
                    ?>
                        <tr>
                            <td scope="row"><?php echo $no++; ?></td>
                            <td><?php echo $k['Nama_kriteria'].' ('.$k['Simbol'].')' ?></td>
                            <td><?php echo $k['sub_kriteria'] ?></td>
                            <td><?php echo $k['nilai'] ?></td>
                            <td>
                                <a class="badge badge-primary" href="<?php echo site_url('subkriteria-edit/' . $k['id_sub_kriteria']); ?> ">Edit</a>

                                <a class="badge badge-danger" href="<?php echo site_url('subkriteria-delete/' . $k['id_sub_kriteria']); ?> " onclick="return(confirm('Data ini ingin dihapus?'))">Hapus</a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
