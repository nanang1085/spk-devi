<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sistem Pendukung Keputusan</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/mdi/css/materialdesignicons.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/iconfonts/font-awesome/css/font-awesome.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/css/vendor.bundle.addons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables/buttons/css/buttons.dataTables.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/shared/style.css">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/demo_1/style.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
    <!-- endinject -->
</head>

<body>
    <div class="container-scroller">
        <!-- partial:../../partials/_navbar.html -->
        <?php $this->load->view('_base/navbar'); ?>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:../../partials/_sidebar.html -->
            <?php $this->load->view('_base/sidebar'); ?>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <?php $this->load->view($view); ?>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:../../partials/_footer.html -->
                <?php $this->load->view('_base/footer'); ?>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <script src="<?php echo base_url(); ?>assets/js/vendor.bundle.base.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/vendor.bundle.addons.js"></script>

    <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap4.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/buttons/js/dataTables.buttons.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/buttons/js/buttons.flash.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/buttons/js/buttons.print.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/buttons/js/buttons.html5.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/jszip/jszip.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/pdfmake/pdfmake.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/pdfmake/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/buttons/js/buttons.bootstrap4.js"></script>
    <!-- inject:js -->
    <script src="<?php echo base_url(); ?>assets/js/shared/off-canvas.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="<?php echo base_url(); ?>assets/js/shared/jquery.cookie.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/shared/misc.js"></script>
    <script>
        $(document).ready(function() {
            $('.tabel').DataTable({
                dom: 'lfrtp',
                // buttons: [
                //     'csv', 'excel', 'pdf', 'print'
                // ],
				"language": {
					"lengthMenu": "Lihat _MENU_ data per halaman",
					"zeroRecords": "Belum ada data",
					"info": "Menampilkan halaman _PAGE_ of _PAGES_",
					"infoEmpty": "Tidak ada data",
					"search":         "Cari:",
					"infoFiltered": "(filtered from _MAX_ total records)",
					"paginate": {
						"first":      "Pertama",
						"last":       "Terakhir",
						"next":       "Selanjutnya",
						"previous":   "Sebelumnya"
					},
				}
            });

			$('.tabel_btn').DataTable({
				dom: 'lfrtBp',
				buttons: [
				    'csv', 'excel', 'pdf', 'print'
				],
				"language": {
					"lengthMenu": "Lihat _MENU_ data per halaman",
					"zeroRecords": "Belum ada data",
					"info": "Menampilkan halaman _PAGE_ of _PAGES_",
					"infoEmpty": "Tidak ada data",
					"search":         "Cari:",
					"infoFiltered": "(filtered from _MAX_ total records)",
					"paginate": {
						"first":      "Pertama",
						"last":       "Terakhir",
						"next":       "Selanjutnya",
						"previous":   "Sebelumnya"
					},
				}
			});

            $(".alert").fadeTo(2000, 500).slideUp(500, function() {
                $(".alert").slideUp(500);
            });

        });
    </script>
</body>

</html>
