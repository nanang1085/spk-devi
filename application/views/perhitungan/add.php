<div class="row">
    <div class="col-4">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Buat Perhitungan</h4>
                    <?php echo form_open('perhitungan-save'); ?>
                    
                    <div class="form-group">
                        <label for="periode">Periode</label>
                        <input type="text" class="form-control" id="periode" name="periode">
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Simpan & Mulai Perhitungan</button>
                    <a class="btn btn-light" href="<?php echo site_url('perhitungan') ?>">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>