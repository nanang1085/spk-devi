<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">

        <div class="card-body">
            <h4>Perhitungan</h4><br>
            <?php
            if ($this->session->flashdata('err_message')) { ?>
                <div class="<?php  echo $this->session->flashdata('err_message')['class']; ?>">
                    <?php  echo $this->session->flashdata('err_message')['message']; ?>
                </div>
            <?php } ?>
            <a href="<?php echo site_url('perhitungan-add'); ?>" class="btn btn-sm btn-primary mb-3">Tambah Data</a>
            <table class="tabel table display compact nowrap">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> Tanggal </th>
                        <th> Periode </th>
                        <th> Aksi </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($perhitungan as $k) {
                    ?>
                        <tr>
                            <td scope="row"><?php echo $no++; ?></td>
                            <td><?= date('d M Y', strtotime($k['tanggal'])) ?></td>
                            <td><?php echo $k['periode'] ?></td>
                            <td>
                            <a class="badge badge-primary" href="<?php echo site_url('perhitungan-view/' . $k['id_perhitungan']); ?> ">Lihat Hasil</a>
                            </td>
                        </tr>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>