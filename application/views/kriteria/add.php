<div class="row">
    <div class="col-4">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Buat Data Kriteria</h4>
                    <?php echo form_open('kriteria/save'); ?>
                    <div class="form-group">
                        <label for="nama">Nama Kriteria</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="simbol">Simbol</label>
                        <input type="text" class="form-control" id="simbol" name="simbol" placeholder="eg. C1, C2, ...">
                    </div>
                    <div class="form-group">
                        <label for="bobot">Bobot</label>
                        <input type="number" class="form-control" id="bobot" name="bobot">
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo site_url('kriteria') ?>">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>