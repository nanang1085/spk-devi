<div class="col-lg-6 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Ubah Data Kriteria</h4>
            <?php echo form_open('kriteria/update'); ?>
            <div class="form-group">
                <label for="nama">Nama Kriteria</label>
                <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $kriteria[0]['Nama_kriteria'] ?>">
                <input type="hidden" name="id_kriteria" value="<?php echo $kriteria[0]['id_kriteria'] ?>">
            </div>
            <div class="form-group">
                <label for="simbol">Simbol</label>
                <input type="text" class="form-control" id="simbol" name="simbol" placeholder="eg. C1, C2, ..." value="<?php echo $kriteria[0]['Simbol'] ?>">
            </div>
            <div class="form-group">
                <label for="bobot">Bobot</label>
                <input type="number" class="form-control" id="bobot" name="bobot" value="<?php echo $kriteria[0]['bobot'] ?>">
            </div>
            <button type="submit" class="btn btn-success mr-2">Ubah Data</button>
            <a class="btn btn-light" href="<?php echo site_url('kriteria') ?>">Kembali</a>
            </form>
        </div>
    </div>
</div>