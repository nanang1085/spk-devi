<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
    </li>
    <li class="nav-item nav-category">Menu</li>
    <li class="nav-item">
      <a class="nav-link" href="<?= site_url('home') ?>">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?= site_url('kriteria') ?>">
        <i class="menu-icon typcn typcn-bell"></i>
        <span class="menu-title">Kriteria</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?= site_url('subkriteria') ?>">
        <i class="menu-icon typcn typcn-bell"></i>
        <span class="menu-title">Range</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?= site_url('alternatif') ?>">
        <i class="menu-icon typcn typcn-bell"></i>
        <span class="menu-title">Alternatif</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?= site_url('penilaian') ?>">
        <i class="menu-icon typcn typcn-bell"></i>
        <span class="menu-title">Penilaian</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?= site_url('perhitungan') ?>">
        <i class="menu-icon typcn typcn-user-outline"></i>
        <span class="menu-title">Perhitungan</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#x" aria-expanded="false" aria-controls="auth">
        <i class="menu-icon typcn typcn-document-add"></i>
        <span class="menu-title">Laporan</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="x">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('lap_alternatif') ?>"> Data Alternatif </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('lap_kriteria') ?>"> Data Kriteria </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('lap_subkriteria') ?>"> Data Range </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('lap_hasil') ?>"> Hasil Perangkingan </a>
          </li>
        </ul>
      </div>
    </li>
    <?php
    if($this->session->userdata['level'] == 'admin'){
    ?>
    <li class="nav-item">
      <a class="nav-link" href="<?= site_url('user') ?>">
        <i class="menu-icon typcn typcn-user-outline"></i>
        <span class="menu-title">User</span>
      </a>
    </li>
    <?php } ?>
  </ul>
</nav>
