<div class="col-lg-6 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Buat Data Siswa</h4>
            <?php echo form_open('alternatif-save'); ?>
                <div class="form-group">
                    <label for="nisn">NISN</label>
                    <input type="text" class="form-control" id="nisn" name="nisn">
                </div>
                <div class="form-group">
                    <label for="Nama_siswa">Nama Siswa</label>
                    <input type="text" class="form-control" id="Nama_siswa" name="Nama_siswa">
                </div>
                <div class="form-group">
                    <label for="asal_sekolah">Asal Sekolah</label>
                    <input type="text" class="form-control" id="asal_sekolah" name="asal_sekolah">
                </div>
                <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                    <input type="text" class="form-control" id="jurusan" name="jurusan">
                </div>
                <button type="submit" class="btn btn-success mr-2">Simpan</button>
                <a class="btn btn-light" href="<?php echo site_url('alternatif') ?>">Kembali</a>
            </form>
        </div>
    </div>
</div>