<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">

        <div class="card-body">
            <h4>Data Siswa</h4><br>
            <?php
            if ($this->session->flashdata('err_message')) { ?>
                <div class="<?php echo $this->session->flashdata('err_message')['class']; ?>">
                    <?php echo $this->session->flashdata('err_message')['message']; ?>
                </div>
            <?php } ?>
            <a href="<?php echo site_url('alternatif-add'); ?>" class="btn btn-sm btn-primary mb-3">Tambah Data</a>
            <a href="<?php echo site_url('alternatif-import'); ?>" class="btn btn-sm btn-primary mb-3">Import Data</a>
            <table class="tabel table display compact nowrap">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> NISN </th>
                        <th> Nama Siswa </th>
                        <th> Sekolah </th>
                        <th> Jurusan </th>
                        <th style="width: 10%;"> Aksi </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($data_alternatif as $k) {
                    ?>
                        <tr>
                            <td scope="row"><?php echo $no++; ?></td>
                            <td><?php echo $k['nisn'] ?></td>
                            <td><?php echo $k['Nama_siswa'] ?></td>
                            <td><?php echo $k['asal_sekolah'] ?></td>
                            <td><?php echo $k['jurusan'] ?></td>
                            <td>
                                <a class="badge badge-primary" href="<?php echo site_url('alternatif-edit/' . $k['id_alternatif']); ?> ">Edit</a>

                                <a class="badge badge-danger" href="<?php echo site_url('alternatif-delete/' . $k['id_alternatif']); ?> " onclick="return(confirm('Data ini ingin dihapus?'))">Hapus</a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
