<div class="col-lg-6 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Ubah Data Siswa</h4>
            <?php echo form_open('alternatif-update'); ?>
            <div class="form-group">
                <label for="nisn">NISN</label>
                <input type="text" class="form-control" id="nisn" name="nisn" value="<?php echo $alternatif[0]['nisn'] ?>">
                <input type="hidden" name="id_alternatif" value="<?php echo $alternatif[0]['id_alternatif'] ?>">
            </div>
            <div class="form-group">
                <label for="Nama_siswa">Nama Siswa</label>
                <input type="text" class="form-control" id="Nama_siswa" name="Nama_siswa" value="<?php echo $alternatif[0]['Nama_siswa'] ?>">
            </div>
            <div class="form-group">
                <label for="asal_sekolah">Asal Sekolah</label>
                <input type="text" class="form-control" id="asal_sekolah" name="asal_sekolah" placeholder="eg. C1, C2, ..." value="<?php echo $alternatif[0]['asal_sekolah'] ?>">
            </div>
            <div class="form-group">
                <label for="jurusan">Jurusan</label>
                <input type="text" class="form-control" id="jurusan" name="jurusan" value="<?php echo $alternatif[0]['jurusan'] ?>">
            </div>
            <button type="submit" class="btn btn-success mr-2">Ubah Data</button>
            <a class="btn btn-light" href="<?php echo site_url('alternatif') ?>">Kembali</a>
            </form>
        </div>
    </div>
</div>