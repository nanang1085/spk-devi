<div class="row">
	<div class="col-4">
		<div class="col-lg-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Import Data Alternatif</h4>
					<?php echo form_open_multipart('alternatif-importresult');?>
					<div class="form-group">
						<label for="nama">Pilih File</label>
						<input type="file" class="form-control" id="file" name="file">
					</div>

					<button type="submit" class="btn btn-success mr-2">Import Data</button>
					<a class="btn btn-light" href="<?php echo site_url('alternatif') ?>">Kembali</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
