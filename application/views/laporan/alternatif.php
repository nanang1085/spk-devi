<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">

        <div class="card-body">
            <h4>Laporan Data Alternatif (Siswa)</h4><br>
          
            <table class="tabel_btn table display compact nowrap">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> NISN </th>
                        <th> Nama Siswa </th>
                        <th> Sekolah </th>
                        <th> Jurusan </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($data_alternatif as $k) {
                    ?>
                        <tr>
                            <td scope="row"><?php echo $no++; ?></td>
                            <td><?php echo $k['nisn'] ?></td>
                            <td><?php echo $k['Nama_siswa'] ?></td>
                            <td><?php echo $k['asal_sekolah'] ?></td>
                            <td><?php echo $k['jurusan'] ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
