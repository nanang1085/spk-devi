<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">

        <div class="card-body">
            <h4>Laporan Data Kriteria</h4><br>
           
            <table class="tabel_btn table display compact nowrap">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> Nama Kriteria </th>
                        <th> Simbol </th>
                        <th> Bobot </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($data_kriteria as $k) {
                    ?>
                        <tr>
                            <td scope="row"><?php echo $no++; ?></td>
                            <td><?php echo $k['Nama_kriteria'] ?></td>
                            <td><?php echo $k['Simbol'] ?></td>
                            <td><?php echo $k['bobot'] ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
