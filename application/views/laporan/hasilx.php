<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">

        <div class="card-body">
            <h4>Laporan Hasil Perangkingan</h4><br>
           
            <table class="tabel table display compact nowrap">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> NISN </th>
                        <th> Nama Siswa </th>
                        <th> Sekolah </th>
                        <th> Jurusan </th>
                        <th> Total Vektor </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($perhitungans as $perhitungan) {
                    ?>
                        <tr>
                            <td scope="row"><?php echo $no++; ?></td>
                            <td><?php echo $perhitungan['nisn'] ?></td>
                            <td><?php echo $perhitungan['Nama_siswa'] ?></td>
                            <td><?php echo $perhitungan['asal_sekolah'] ?></td>
                            <td><?php echo $perhitungan['jurusan'] ?></td>
                            <td><?php echo $perhitungan['nilai'] ?></td>
                        </tr>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>