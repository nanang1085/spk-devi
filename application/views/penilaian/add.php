<?php echo form_open('penilaian-save'); ?>

<div class="col-lg-8 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-4">Penilaian</h4>

            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>NISN</th>
                        <th>Nama Siswa</th>
                        <th>Asal Sekolah</th>
                        <th>Jurusan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $siswa[0]['nisn'] ?></td>
                        <td><?= $siswa[0]['Nama_siswa'] ?></td>
                        <td><?= $siswa[0]['asal_sekolah'] ?></td>
                        <td><?= $siswa[0]['jurusan'] ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-lg-8 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-4">Kriteria</h4>

            <?php
            foreach ($kriteria as $i => $k) {
            ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <label> <?= $k['nama_kriteria'] ?> </label>
                            <input type="hidden" name="penilaian[<?= $i ?>][id_kriteria]" value="<?= $k['id_kriteria'] ?>">
                            <input type="hidden" name="penilaian[<?= $i ?>][id_alternatif]" value="<?= $siswa[0]['id_alternatif'] ?>">
                        </div>
                        <div class="col-6">
                            <select name="penilaian[<?= $i ?>][id_sub_kriteria]" class="form-control">
                                <?php foreach ($k['sub_kriteria'] as $s) { ?>
                                    <option value="<?= $s['id_sub_kriteria'] ?>">
                                        <?= $s['sub_kriteria'] . ' (' . $s['nilai'] . ')' ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <button type="submit" class="btn btn-success"> Simpan Data</button>
                <a href="<?= site_url('penilaian') ?>" class="btn btn-light"> Kembali</a>
            </div>
        </div>
    </div>
</div>
</form>