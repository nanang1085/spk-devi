<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">

        <div class="card-body">
            <h4>Penilaian</h4><br>

            <?php
            if ($this->session->flashdata('err_message')) { ?>
                <div class="<?php echo $this->session->flashdata('err_message')['class']; ?>">
                    <?php echo $this->session->flashdata('err_message')['message']; ?>
                </div>
            <?php } ?>

            <table class="tabel table display compact nowrap">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> NISN </th>
                        <th> Nama Siswa </th>
                        <th> Status </th>
                        <th style="width: 10%;"> Aksi </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($data_alternatif as $k) {
                    ?>
                        <tr>
                            <td scope="row"><?php echo $no++; ?></td>
                            <td><?php echo $k['nisn'] ?></td>
                            <td><?php echo $k['Nama_siswa'] ?></td>
                            <td>
                                <?php
                                if ($k['status'] == true) {
                                ?>
                                    <i class="fa fa-check text-success" title="Sudah input penilaian."></i>
                                <?php
                                }
                                ?>
                            </td>
                            <td>

                                <?php
                                if ($k['status'] == true) {
                                ?>
                                    <a class="badge badge-warning" href="<?php echo site_url('penilaian-edit/' . $k['id_alternatif']); ?> ">Edit</a>
                                <?php
                                } else {
                                ?>
                                    <a class="badge badge-primary" href="<?php echo site_url('penilaian-add/' . $k['id_alternatif']); ?> ">Input</a>
                                <?php } ?>
                                
                                <a class="badge badge-danger" href="<?php echo site_url('penilaian-delete/' . $k['id_alternatif']); ?> ">Reset</a>
                            </td>
                        </tr>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>