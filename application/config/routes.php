<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'LoginController/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// user
$route['user'] = 'UserController/index';
$route['user_save'] = 'UserController/save';
$route['user_add'] = 'UserController/add';
$route['user_delete/(:num)'] = 'UserController/delete/$1';

// auth
$route['login'] = 'LoginController/index';
$route['register'] = 'LoginController/register';
$route['registersave'] = 'LoginController/registersave';
$route['login/masuk'] = 'LoginController/masuk';
$route['logout'] = 'LoginController/logout';

// dashboard
$route['home'] = 'HomeController/index';

// kriteria
$route['kriteria'] = 'KriteriaController/index';
$route['kriteria/add'] = 'KriteriaController/add';
$route['kriteria/save'] = 'KriteriaController/save';
$route['kriteria/update'] = 'KriteriaController/update';
$route['kriteria/delete/(:num)'] = 'KriteriaController/delete/$1';
$route['kriteria/edit/(:num)'] = 'KriteriaController/edit/$1';

// subkriteria
$route['subkriteria'] = 'SubKriteriaController/index';
$route['subkriteria-add'] = 'SubKriteriaController/add';
$route['subkriteria-save'] = 'SubKriteriaController/save';
$route['subkriteria-update'] = 'SubKriteriaController/update';
$route['subkriteria-delete/(:num)'] = 'SubKriteriaController/delete/$1';
$route['subkriteria-edit/(:num)'] = 'SubKriteriaController/edit/$1';

// alternatif
$route['alternatif'] = 'AlternatifController/index';
$route['alternatif-add'] = 'AlternatifController/add';
$route['alternatif-save'] = 'AlternatifController/save';
$route['alternatif-update'] = 'AlternatifController/update';
$route['alternatif-import'] = 'AlternatifController/import';
$route['alternatif-importresult'] = 'AlternatifController/importresult';
$route['alternatif-delete/(:num)'] = 'AlternatifController/delete/$1';
$route['alternatif-edit/(:num)'] = 'AlternatifController/edit/$1';

// penilaian
$route['penilaian'] = 'PenilaianController/index';
$route['penilaian-add/(:num)'] = 'PenilaianController/add/$1';
$route['penilaian-save'] = 'PenilaianController/save';
$route['penilaian-update'] = 'PenilaianController/update';
$route['penilaian-delete/(:num)'] = 'PenilaianController/delete/$1';
$route['penilaian-edit/(:num)'] = 'PenilaianController/edit/$1';

// perhitungan
$route['perhitungan'] = 'PerhitunganController/index';
$route['perhitungan-add'] = 'PerhitunganController/add';
$route['perhitungan-save'] = 'PerhitunganController/save';
$route['perhitungan-view/(:num)'] = 'PerhitunganController/view/$1';

// laporan
$route['lap_alternatif'] = 'LaporanController/alternatif';
$route['lap_kriteria'] = 'LaporanController/kriteria';
$route['lap_subkriteria'] = 'LaporanController/subkriteria';
$route['lap_hasil'] = 'LaporanController/hasil';
$route['lap_view/(:num)'] = 'LaporanController/viewhasil/$1';
