<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penilaian extends CI_Model
{
    protected $table = 'tabel_penilaian';
    protected $key = 'id_penilaian';
    protected $alternatif_key = 'id_alternatif';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('kriteria');
        $this->load->model('subkriteria');
        $this->load->model('alternatif');
    }

    protected function getPenilaianByKriteriaAndSubkriteria($id_kriteria, $id_subkriteria, $id_alternatif)
    {
        $this->db->where('id_kriteria', $id_kriteria);
        $this->db->where('id_subkriteria', $id_subkriteria);
        $this->db->where('id_alternatif', $id_alternatif);
        return $this->db->get($this->table)->result_array();
    }

    // get data kriteria dan subkriteria-nya
    public function getKriteriaAndSub($id_alternatif = null)
    {
        $data = array();

        foreach ($this->kriteria->getAllKriteria() as $kriteria) {

            $k = array();
            $k['id_kriteria'] = $kriteria['id_kriteria'];
            $k['nama_kriteria'] = $kriteria['Nama_kriteria'];
            $k['simbol'] = $kriteria['Simbol'];

            $subs = array();
            foreach ($this->subkriteria->getByIdKriteria($kriteria['id_kriteria']) as $sub) {
                // cek by id_kriteria, sub kriteria, id alternatid di penilaian, 
                // kalo ada datanya, selected = true di masing2 dropdown
                $p = $this->getPenilaianByKriteriaAndSubkriteria($sub['id_kriteria'], $sub['id_sub_kriteria'], $id_alternatif);

                $s = array();
                $s['id_sub_kriteria'] = $sub['id_sub_kriteria'];
                $s['id_kriteria'] = $sub['id_kriteria'];
                $s['sub_kriteria'] = $sub['sub_kriteria'];
                $s['nilai'] = $sub['nilai'];
                $s['selected'] = empty($p) ? false : true;

                array_push($subs, $s);
            }

            $k['sub_kriteria'] = $subs;

            array_push($data, $k);
        }

        return $data;
    }

    // get all penilaian group by alternatif
    public function getPenilaianGroupByAlternatif(){
        $this->db->select('id_alternatif');
        $this->db->group_by($this->alternatif_key);
        return $this->db->get($this->table)->result_array();
    }

    // get all penilaian
    public function getAllPenilaian()
    {
        return $this->db->get($this->table)->result_array();
    }

    // get all data dari penilaian joined
    public function getAll()
    {
        $this->db->select('*');
        $this->db->from('tabel_penilaian p');
        $this->db->join('data_kriteria dk', 'dk.id_kriteria=p.id_kriteria', 'left');
        $this->db->join('data_alternatif da', 'da.id_alternatif=p.id_alternatif', 'left');
        $this->db->join('data_alternatif da', 'da.id_alternatif=p.id_alternatif', 'left');
        $this->db->order_by('dk.Simbol', 'asc');
        return $this->db->get()->result_array();
    }

    // list data alternatif join penilaian
    public function getAlternatifPenilaian()
    {

        $data = array();
        foreach ($this->alternatif->getAll() as $k) {

            // cek apakah kriteria sudah pernah diinput di penilaian
            $cek = $this->getByIdAlternatif($k['id_alternatif']);

            $a = array();
            $a['id_alternatif'] = $k['id_alternatif'];
            $a['nisn'] = $k['nisn'];
            $a['Nama_siswa'] = $k['Nama_siswa'];
            $a['status'] = !empty($cek) ? true : false;

            array_push($data, $a);
        }
        return $data;
    }
    // get penilaian by id_penilaian
    public function getById($id)
    {
        $this->db->where($this->key, $id);
        return $this->db->get($this->table)->result_array();
    }

    // get penilaian by id_alternatif
    public function getByIdAlternatif($id)
    {
        $this->db->where($this->alternatif_key, $id);
        return $this->db->get($this->table)->result_array();
    }

    // simpan data penilaian
    public function save($data)
    {
        $out = false;
        //echo '<pre>';print_r($data);die();
        foreach ($data as $d) {

            $subkriteria = $this->subkriteria->getById($d['id_sub_kriteria']);

            $array = array(
                'id_alternatif' => $d['id_alternatif'],
                'id_kriteria' => $d['id_kriteria'],
                'id_subkriteria' => $d['id_sub_kriteria'],
                'predikat' => $subkriteria[0]['nilai'],
            );

            if ($this->db->insert($this->table, $array)) {
                $out = true;
            }
        }

        return $out;
    }

    public function update($data)
    {
        $out = false;

        // delete all by id_alternatif
        if ($this->delete($data[0]['id_alternatif'])) {
            $out = true;
        }

        // re-insert data
        if ($this->save($data)) {
            $out = true;
        }
        return $out;
    }

    public function delete($id)
    {
        $out = false;
        $this->db->where($this->alternatif_key, $id);
        if ($this->db->delete($this->table)) {
            $out = true;
        }
        return $out;
    }
}
