<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alternatif extends CI_Model
{
    protected $table = 'data_alternatif';
    protected $key = 'id_alternatif';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAll()
    {
        return $this->db->get($this->table)->result_array();
    }

    public function getById($id)
    {
        $this->db->where($this->key, $id);
        return $this->db->get($this->table)->result_array();
    }

    public function save($data)
    {
        $out = false;
        $array = array(
            'Nama_siswa' => $data['Nama_siswa'],
            'nisn' => $data['nisn'],
            'jurusan' => $data['jurusan'],
            'asal_sekolah' => $data['asal_sekolah']
        );
        if ($this->db->insert($this->table, $array)) {
            $out = true;
        }
        return $out;
    }

	public function saveMulti($data)
	{
		$out = 0;

		foreach($data as $d){

			$array = array(
				'Nama_siswa' => $d['Nama_siswa'],
				'nisn' => $d['nisn'],
				'jurusan' => $d['jurusan'],
				'asal_sekolah' => $d['asal_sekolah']
			);

			if ($this->db->insert($this->table, $array)) {
				$out++;
			}
		}

		return $out;
	}

    public function update($data)
    {
        $out = false;
        $array = array(
            'Nama_siswa' => $data['Nama_siswa'],
            'nisn' => $data['nisn'],
            'jurusan' => $data['jurusan'],
            'asal_sekolah' => $data['asal_sekolah']
        );
        $this->db->where($this->key, $data[$this->key]);
        if ($this->db->update($this->table, $array)) {
            $out = true;
        }
        return $out;
    }

    public function delete($id)
    {
        $out = false;
        $this->db->where($this->key, $id);
        if ($this->db->delete($this->table)) {
            $out = true;
        }
        return $out;
    }
}
