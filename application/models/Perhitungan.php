<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perhitungan extends CI_Model
{
    protected $table = 'perhitungan';
    protected $key = 'id_perhitungan';
    protected $table_hasil = 'hasil';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    // get all data perhitungan
    public function getAll()
    {
        return $this->db->get($this->table)->result_array();
    }

    // get data hasil perhitungan joined alternatif by id_perhitungan
    public function getPerhitunganAlternatif($id){
        $this->db->select('*');
        $this->db->from('hasil');
        $this->db->join('data_alternatif da', 'da.id_alternatif=hasil.id_alternatif', 'left');
        $this->db->where($this->key, $id);
        //$this->db->order_by('da.Nama_siswa', 'asc');
        $this->db->order_by('hasil.nilai', 'desc');
        return $this->db->get()->result_array();
    }

    // get data hasil perhitungan joined alternatif, perhitungan
    public function getAllPerhitunganAlternatif(){
        $this->db->select('*');
        $this->db->from('hasil');
        $this->db->join('data_alternatif da', 'da.id_alternatif=hasil.id_alternatif', 'left');
        $this->db->join('perhitungan p', 'p.id_perhitungan = hasil.id_perhitungan', 'left');
        $this->db->order_by('hasil.nilai', 'desc');
        return $this->db->get()->result_array();
    }

    // simpan ke perhitungan
    protected function save_perhitungan($post)
    {
        $array = array(
            'periode' => $post['periode'],
            'tanggal' => date('Y-m-d')
        );

        $this->db->insert($this->table, $array);
        return $this->db->insert_id();
    }

    // simpan ke hasil
    protected function save_hasil($array)
    {
        return $this->db->insert($this->table_hasil, $array);
    }

    // simpan perhitungan
    public function save($post, $hasil_vektor)
    {
        $out = false;
        // simpan ke perhitungan
        $h = $this->save_perhitungan($post);

        // simpan ke hasil
        foreach ($hasil_vektor as $hasil) {

            $array = array(
                'id_perhitungan' => $h,
                'id_alternatif' => $hasil['id_alternatif'],
                'nilai' => $hasil['total_vektor']
            );

            if ($this->save_hasil($array)) {
                $out = true;
            }
        }
        return $out;
    }
}
