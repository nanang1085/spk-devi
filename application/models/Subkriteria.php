<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Subkriteria extends CI_Model
{
    protected $table = 'data_kriteria_sub';
    protected $key = 'id_sub_kriteria';
    protected $foreign_key = 'id_kriteria';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAll()
    {
        $this->db->select('*');
        $this->db->from('data_kriteria_sub dks'); 
        $this->db->join('data_kriteria dk', 'dk.id_kriteria=dks.id_kriteria', 'left');
        $this->db->order_by('dk.Simbol','asc');  
        return $this->db->get()->result_array();
    }

    public function getById($id)
    {
        $this->db->where($this->key, $id);
        return $this->db->get($this->table)->result_array();
    }

    public function getByIdKriteria($id)
    {
        $this->db->where($this->foreign_key, $id);
        return $this->db->get($this->table)->result_array();
    }

    public function save($data)
    {
        $out = false;
        $array = array(
            'id_kriteria' => $data['id_kriteria'],
            'sub_kriteria' => $data['sub_kriteria'],
            'nilai' => $data['nilai'],
        );
        if ($this->db->insert($this->table, $array)) {
            $out = true;
        }
        return $out;
    }

    public function update($data)
    {
        $out = false;
        $array = array(
            'id_kriteria' => $data['id_kriteria'],
            'sub_kriteria' => $data['sub_kriteria'],
            'nilai' => $data['nilai'],
        );
        $this->db->where($this->key, $data[$this->key]);
        if ($this->db->update($this->table, $array)) {
            $out = true;
        }
        return $out;
    }

    public function delete($id)
    {
        $out = false;
        $this->db->where($this->key, $id);
        if ($this->db->delete($this->table)) {
            $out = true;
        }
        return $out;
    }
}
