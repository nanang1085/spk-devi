<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{

    protected $table = 'admin';
    protected $key = 'NIP';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAll(){
        return $this->db->get($this->table)->result_array();
    }

    public function cekDataExist($nip){
        $this->db->where($this->key, $nip);
        return $this->db->get($this->table)->result_array(); 
    }

    public function saveregister($data)
    {
        $out = false;
        $array = array(
            'NIP' => $data['nip'],
            'Username' => $data['username'],
            'Password' => $data['password'],
            'Nama' => $data['nama'],
            'level'=>$data['level']
        );
        if ($this->db->insert($this->table, $array)) {
            $out = true;
        }
        return $out;
    }

    /**
     * Cek Login
     */
    public function cekLogin($post)
    {
        $this->db->where('username', $post['username']);
        $this->db->where('password', $post['password']);

        return $this->db->get($this->table)->result_array();
    }

    public function isNotLogin(){
        return $this->session->userdata('user_logged') == false;
    }

    public function delete($id)
    {
        $out = false;
        $this->db->where('nip', $id);
        if ($this->db->delete($this->table)) {
            $out = true;
        }
        return $out;
    }
}
