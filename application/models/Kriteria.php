<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kriteria extends CI_Model
{
    protected $table = 'data_kriteria';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAllKriteria()
    {
        return $this->db->get($this->table)->result_array();
    }

    public function getKriteriaById($id)
    {
        $this->db->where('id_kriteria', $id);
        return $this->db->get($this->table)->result_array();
    }

    public function save($data)
    {
        $out = false;
        $array = array(
            'Nama_kriteria' => $data['nama'],
            'Simbol' => $data['simbol'],
            'bobot' => $data['bobot'],
        );
        if ($this->db->insert($this->table, $array)) {
            $out = true;
        }
        return $out;
    }

    public function update($data)
    {
        $out = false;
        $array = array(
            'Nama_kriteria' => $data['nama'],
            'Simbol' => $data['simbol'],
            'bobot' => $data['bobot'],
        );
        $this->db->where('id_kriteria', $data['id_kriteria']);
        if ($this->db->update($this->table, $array)) {
            $out = true;
        }
        return $out;
    }

    public function delete($id)
    {
        $out = false;
        $this->db->where('id_kriteria', $id);
        if ($this->db->delete($this->table)) {
            $out = true;
        }
        return $out;
    }
}
