<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model("Admin_model");
        if ($this->Admin_model->isNotLogin()) redirect('login');

    }

    public function index()
    {
        $data = array();
        $data['view'] = 'user/index';
        $data['users'] = $this->Admin_model->getAll();
        $this->load->view('main', $data);
    }

    public function add(){
        $data = array();
        $data['view'] = 'user/add';
        $this->load->view('main', $data);
    }

    public function save(){
        $post = $this->input->post();
        // cek data
        $exist = $this->Admin_model->cekDataExist($post['nip']);
        // cocokkan password dan repassword
        if($post['password'] != $post['repassword']){
            $this->session->set_flashdata('err_message', array('message' => 'Password tidak sama. Password dan Ulangi Password harus sama!', 'class' => 'alert alert-warning'));
            redirect('user_add');
        }
        // cek apakah NIP sudah terdaftar
        if(!empty($exist)){
            $this->session->set_flashdata('err_message', array('message' => 'NIP sudah terdaftar!', 'class' => 'alert alert-warning'));
            redirect('user_add');
        }

        // jika tidak terdaftar dan password sama
        if($this->Admin_model->saveregister($post)){
            $this->session->set_flashdata('err_message', array('message' => 'User berhasil dibuat.', 'class' => 'alert alert-success'));
            redirect('user');
        }
    }

    public function delete($id)
    {
        if ($this->Admin_model->delete($id)) {
            $this->session->set_flashdata('err_message', array('message' => 'Berhasil menghapus data.', 'class' => 'alert alert-success'));
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Gagal menghapus data', 'class' => 'alert alert-danger'));
        }

        redirect('user');
    }
}