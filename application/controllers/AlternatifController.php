<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'third_party/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class AlternatifController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('alternatif');
		$this->load->model("Admin_model");
		if ($this->Admin_model->isNotLogin()) redirect('login');
	}

	public function index()
	{
		$data = array();
		$data['view'] = 'alternatif/index';
		$data['data_alternatif'] = $this->alternatif->getAll();
		$this->load->view('main', $data);
	}

	public function add()
	{
		$data = array();
		$data['view'] = 'alternatif/add';
		$this->load->view('main', $data);
	}

	public function edit($id)
	{
		$data = array();
		$data['view'] = 'alternatif/edit';
		$data['alternatif'] = $this->alternatif->getById($id);
		$this->load->view('main', $data);
	}

	public function save()
	{

		if ($this->alternatif->save($this->input->post())) {
			$this->session->set_flashdata('err_message', array('message' => 'Berhasil menyimpan data.', 'class' => 'alert alert-success'));
			redirect('alternatif');
		} else {
			$this->session->set_flashdata('err_message', array('message' => 'Gagal menyimpan data', 'class' => 'alert alert-danger'));
			redirect('alternatif-add');
		}
	}

	public function update()
	{
		if ($this->alternatif->update($this->input->post())) {
			$this->session->set_flashdata('err_message', array('message' => 'Berhasil mengubah data.', 'class' => 'alert alert-success'));
			redirect('alternatif');
		} else {
			$this->session->set_flashdata('err_message', array('message' => 'Gagal mengubah data', 'class' => 'alert alert-danger'));
			redirect('alternatif-edit/' . $this->input->post('id_alternatif'));
		}
	}

	public function delete($id)
	{

		if ($this->alternatif->delete($id)) {
			$this->session->set_flashdata('err_message', array('message' => 'Berhasil menghapus data.', 'class' => 'alert alert-success'));
		} else {
			$this->session->set_flashdata('err_message', array('message' => 'Gagal menghapus data', 'class' => 'alert alert-danger'));
		}

		redirect('alternatif');
	}

	public function import()
	{
		$data = array();
		$data['view'] = 'alternatif/import';
		$this->load->view('main', $data);
	}

	public function importresult()
	{

		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'xls|xlsx|ods';
		$config['file_name'] = 'doc' . time();

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file')) {

			$error = array('error' => $this->upload->display_errors());
			print_r($error);
			die();

		} else {

			$file = $this->upload->data();
			$path = 'upload/' . $file['file_name'];

			$reader = ReaderEntityFactory::createReaderFromFile($path);
			$reader->open($path);

			foreach ($reader->getSheetIterator() as $sheet) {

				$numRow = 1;
				$save = array();

				foreach ($sheet->getRowIterator() as $row) {

					if ($numRow > 1) {
						//ambil cell
						$cells = $row->getCells();

						$data = array(
							'nisn' => $cells[0]->getValue(),
							'Nama_siswa' => $cells[1]->getValue(),
							'asal_sekolah' => $cells[2]->getValue(),
							'jurusan' => $cells[3]->getValue()
						);

						//tambahkan array $data ke $save
						array_push($save, $data);
					}

					$numRow++;
				}
			}
			// simpan data
			$count = $this->alternatif->saveMulti($save);
			$reader->close();

			$this->session->set_flashdata('err_message', array('message' => $count . ' data disimpan.', 'class' => 'alert alert-success'));
			redirect('alternatif');

		}
	}
}
