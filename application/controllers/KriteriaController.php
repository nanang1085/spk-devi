<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KriteriaController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('kriteria');
        $this->load->model("Admin_model");
        if ($this->Admin_model->isNotLogin()) redirect('login');

    }

    public function index()
    {

        $data = array();
        $data['view'] = 'kriteria/index';
        $data['data_kriteria'] = $this->kriteria->getAllKriteria();
        $this->load->view('main', $data);
    }

    public function add()
    {
        $data = array();
        $data['view'] = 'kriteria/add';
        $this->load->view('main', $data);
    }

    public function edit($id)
    {
        $data = array();
        $data['view'] = 'kriteria/edit';
        $data['kriteria'] = $this->kriteria->getKriteriaById($id);
        $this->load->view('main', $data);
    }

    public function save()
    {
        if ($this->kriteria->save($this->input->post())) {
            $this->session->set_flashdata('err_message', array('message' => 'Berhasil menyimpan data.', 'class' => 'alert alert-success'));
            redirect('kriteria');
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Gagal menyimpan data', 'class' => 'alert alert-danger'));
            redirect('kriteria/add');
        }
    }

    public function update()
    {
        if ($this->kriteria->update($this->input->post())) {
            $this->session->set_flashdata('err_message', array('message' => 'Berhasil mengubah data.', 'class' => 'alert alert-success'));
            redirect('kriteria');
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Gagal mengubah data', 'class' => 'alert alert-danger'));
            redirect('kriteria/edit/' . $this->input->post('id_kriteria'));
        }
    }

    public function delete($id)
    {

        if ($this->kriteria->delete($id)) {
            $this->session->set_flashdata('err_message', array('message' => 'Berhasil menghapus data.', 'class' => 'alert alert-success'));
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Gagal menghapus data', 'class' => 'alert alert-danger'));
        }

        redirect('kriteria');
    }
}
