<?php
defined('BASEPATH') or exit('No direct script access allowed');

class HomeController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model("Admin_model");
        if ($this->Admin_model->isNotLogin()) redirect('login');
    }
    public function index()
    {
       // print_r($this->session->userdata('Username'));die();
        $data = array();
        $data['view'] = 'home';
        $this->load->view('main', $data);
    }
}
