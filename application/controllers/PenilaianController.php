<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PenilaianController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('penilaian');
        $this->load->model('alternatif');
        $this->load->model("Admin_model");
        if ($this->Admin_model->isNotLogin()) redirect('login');

    }

    public function index()
    {
        $data = array();
        $data['view'] = 'penilaian/index';
        $data['data_alternatif'] = $this->penilaian->getAlternatifPenilaian();
        $this->load->view('main', $data);
    }

    // buat data penilaian by id_alternatif
    public function add($id)
    {
        $data = array();
        $data['view'] = 'penilaian/add';
        $data['siswa'] = $this->alternatif->getById($id);
        $data['kriteria'] = $this->penilaian->getKriteriaAndSub();
        $this->load->view('main', $data);
    }

    // ubah penilaian by id_alternatif
    public function edit($id)
    {
        $data = array();
        $data['view'] = 'penilaian/edit';
        $data['siswa'] = $this->alternatif->getById($id);
        $data['kriteria'] = $this->penilaian->getKriteriaAndSub($id);
        $this->load->view('main', $data);
    }

    // simpan penilaian
    public function save()
    {
        // cek data alternatif sudah pernah diinput atau belum
        // jika sudah pernah diinput, tampilkan error
        // jika belum pernah, simpan ke database
        $post = $this->input->post('penilaian');

        if (empty($this->penilaian->getByIdAlternatif($post[0]['id_alternatif']))) {
            if ($this->penilaian->save($post)) {
                $this->session->set_flashdata('err_message', array('message' => 'Berhasil menyimpan data.', 'class' => 'alert alert-success'));
                redirect('penilaian');
            } else {
                $this->session->set_flashdata('err_message', array('message' => 'Gagal menyimpan data', 'class' => 'alert alert-danger'));
                redirect('penilaian');
            }
        }

        $this->session->set_flashdata('err_message', array('message' => 'Data sudah pernah diinputkan sebelumnya.', 'class' => 'alert alert-warning'));
        redirect('penilaian');
    }

    // update penilaian
    public function update()
    {
        if ($this->penilaian->update($this->input->post('penilaian'))) {
            $this->session->set_flashdata('err_message', array('message' => 'Berhasil menyimpan data.', 'class' => 'alert alert-success'));
            redirect('penilaian');
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Gagal menyimpan data', 'class' => 'alert alert-danger'));
            redirect('penilaian');
        }
    }

    // reset penilaian by id_alternatif
    public function delete($id)
    {
        if ($this->penilaian->delete($id)) {
            $this->session->set_flashdata('err_message', array('message' => 'Berhasil menghapus data penilaian.', 'class' => 'alert alert-success'));
            redirect('penilaian');
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Gagal menghapus data penilaian.', 'class' => 'alert alert-danger'));
            redirect('penilaian');
        }
    }
}
