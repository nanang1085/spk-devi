<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SubKriteriaController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('subkriteria');
        $this->load->model('kriteria');
        $this->load->model("Admin_model");
        if ($this->Admin_model->isNotLogin()) redirect('login');

    }

    public function index()
    {
        $data = array();
        $data['view'] = 'subkriteria/index';
        $data['data_subkriteria'] = $this->subkriteria->getAll();
        $this->load->view('main', $data);
    }

    public function add()
    {
        $data = array();
        $data['view'] = 'subkriteria/add';
        $data['data_kriteria'] = $this->kriteria->getAllKriteria();
        $this->load->view('main', $data);
    }

    public function edit($id)
    {
        $data = array();
        $data['view'] = 'subkriteria/edit';
        $data['dks'] = $this->subkriteria->getById($id);
        $data['data_kriteria'] = $this->kriteria->getAllKriteria();
        $this->load->view('main', $data);
    }

    public function save()
    {
        if ($this->subkriteria->save($this->input->post())) {
            $this->session->set_flashdata('err_message', array('message' => 'Berhasil menyimpan data.', 'class' => 'alert alert-success'));
            redirect('subkriteria');
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Gagal menyimpan data', 'class' => 'alert alert-danger'));
            redirect('subkriteria/add');
        }
    }

    public function update()
    {
        if ($this->subkriteria->update($this->input->post())) {
            $this->session->set_flashdata('err_message', array('message' => 'Berhasil mengubah data.', 'class' => 'alert alert-success'));
            redirect('subkriteria');
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Gagal mengubah data', 'class' => 'alert alert-danger'));
            redirect('subkriteria/edit/' . $this->input->post('id_sub_kriteria'));
        }
    }

    public function delete($id)
    {

        if ($this->subkriteria->delete($id)) {
            $this->session->set_flashdata('err_message', array('message' => 'Berhasil menghapus data.', 'class' => 'alert alert-success'));
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Gagal menghapus data', 'class' => 'alert alert-danger'));
        }

        redirect('subkriteria');
    }
}
