<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PerhitunganController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('perhitungan');
        $this->load->model('penilaian');
        $this->load->model("Admin_model");
        if ($this->Admin_model->isNotLogin()) redirect('login');

    }

    // cari dan konversi bobot kriteria
    protected function bobotKriteria($id_kriteria)
    {
        $k = $this->kriteria->getKriteriaById($id_kriteria);
        return $k[0]['bobot'] / 100;
    }

    // hitung vektor
    protected function hitungVektor()
    {
        $total_vektor_s = 0;
        $out = array();
        $data_vektor_s = array();
        
        // loop penilaian berdasarkan jumlah alternatif yang ada di penilaian
        foreach($this->penilaian->getPenilaianGroupByAlternatif() as $alt){
  
            $b = array();
            $b['id_alternatif'] = $alt['id_alternatif'];

            $p = array();
            // loop penilaian berdasarkan masing2 id_alternatif di penilaian
            foreach ($this->penilaian->getByIdAlternatif($alt['id_alternatif']) as $penilaian) {
                // exponentiation
                $exponentiation = pow($penilaian['predikat'], $this->bobotKriteria($penilaian['id_kriteria']));
    
                $a = array();                
                $a = $exponentiation;
                array_push($p, $a);
            }
            
            // calculate exponentiation (dikali). total vektor s
            $subtotal_vektor_s = array_product($p);
            $b['total_vektor']= $subtotal_vektor_s;
            // push to array
            array_push($data_vektor_s, $b);
            // total vektor s
            $total_vektor_s = $total_vektor_s + $subtotal_vektor_s;
        }

        // hitung vektor v
        foreach($data_vektor_s as $d){
            
            $c = array();
            $c['id_alternatif'] = $d['id_alternatif'];
            $c['total_vektor'] = $d['total_vektor'] / $total_vektor_s;

            array_push($out, $c);
        }

        return $out;
    }

    // index perhitungan
    public function index(){
        $data = array();
        $data['view'] = 'perhitungan/index';
        $data['perhitungan'] = $this->perhitungan->getAll();
        $this->load->view('main', $data);
    }

    // buat perhitungan
    public function add(){

        $data = array();
        $data['view'] = 'perhitungan/add';
        $this->load->view('main', $data);
    }

    // simpan perhitungan dan hasil
    public function save(){
        
        if ($this->perhitungan->save($this->input->post(), $this->hitungVektor())) {
            $this->session->set_flashdata('err_message', array('message'=>'Selesai melakukan perhitungan dan data sudah disimpan.','class'=>'alert alert-success'));
            redirect('perhitungan');
        }else {
            $this->session->set_flashdata('err_message', array('message'=>'Gagal! Terjadi kesalahan.','class'=>'alert alert-danger'));
            redirect('perhitungan');
        }
    }

     // tampil data hasil perhitungan by id_perhitungan
     public function view($id){

        $data = array();
        $data['view'] = 'perhitungan/view';
        $data['perhitungans'] = $this->perhitungan->getPerhitunganAlternatif($id);
        $this->load->view('main', $data);
    }
}
