<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LaporanController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('alternatif');
        $this->load->model('kriteria');
        $this->load->model('subkriteria');
        $this->load->model('perhitungan');
        $this->load->model("Admin_model");
        if ($this->Admin_model->isNotLogin()) redirect('login');

    }

    public function alternatif()
    {
        $data = array();
        $data['view'] = 'laporan/alternatif';
        $data['data_alternatif'] = $this->alternatif->getAll();
        $this->load->view('main', $data);
    }

    public function kriteria()
    {
        $data = array();
        $data['view'] = 'laporan/kriteria';
        $data['data_kriteria'] = $this->kriteria->getAllKriteria();
        $this->load->view('main', $data);
    }

    public function subkriteria()
    {
        $data = array();
        $data['view'] = 'laporan/sub_kriteria';
        $data['data_subkriteria'] = $this->subkriteria->getAll();
        $this->load->view('main', $data);
    }

    public function hasil()
    {
        $data = array();
        $data['view'] = 'laporan/hasil';
        $data['perhitungan'] = $this->perhitungan->getAll();
        $this->load->view('main', $data);
    }

    // tampil data hasil perhitungan by id_perhitungan
    public function viewhasil($id){

        $data = array();
        $data['view'] = 'laporan/view';
        $data['perhitungans'] = $this->perhitungan->getPerhitunganAlternatif($id);
        $this->load->view('main', $data);
    }
}