<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LoginController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->model('Admin_model');
    }

    /**
     * index
     */
    public function index()
    {
        $this->load->view('auth/login');
    }

    public function register(){
        $this->load->view('auth/register');
    }

    public function registersave(){

        $post = $this->input->post();
        // cek data
        $exist = $this->Admin_model->cekDataExist($post['nip']);
        // cocokkan password dan repassword
        if($post['password'] != $post['repassword']){
            $this->session->set_flashdata('err_message', array('message' => 'Password tidak sama. Password dan Ulangi Password harus sama!', 'class' => 'alert alert-warning'));
            redirect('register');
        }
        // cek apakah NIP sudah terdaftar
        if(!empty($exist)){
            $this->session->set_flashdata('err_message', array('message' => 'NIP sudah terdaftar!', 'class' => 'alert alert-warning'));
            redirect('register');
        }

        // jika tidak terdaftar dan password sama
        if($this->Admin_model->saveregister($post)){
            $this->session->set_flashdata('err_message', array('message' => 'Registrasi berhasil. Silahkan login.', 'class' => 'alert alert-warning'));
            redirect('login');
        }
    }

    /**
     * login
     */
    public function masuk()
    {
        $post = $this->input->post();
        $cek = $this->Admin_model->cekLogin($post);

        if (!empty($cek)) {
            $this->session->set_userdata(['username' => $cek[0]['Username'], 'level' => $cek[0]['level'], 'user_logged' => true]);
            redirect('home');
        } else {
            $this->session->set_flashdata('err_message', array('message' => 'Username/ Password salah!', 'class' => 'alert alert-warning'));
            redirect('login');
        }
    }

    public function logout()
    {
        // hancurkan semua sesi
        $this->session->sess_destroy();
        redirect('login');
    }
}
